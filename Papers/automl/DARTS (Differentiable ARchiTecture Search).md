DARTS (Differentiable ARchiTecture Search).


[Оглавление](../../Papers/automl/Оглавление.md)
# Intro
-Rl
-EVO
+Grad descent
Подбирают для текста и для изображений, двухууровневая оптимизация
# Search Space
Подход основан на ячейках. Ячейка представляет собой ациклических граф, упорядоченную последовательность N узлов. Узел представляет собой состояние x(i), latent representation, например fearture map, а рёбра (i,j) ассоциируются с операторами o(i,j) и трансформациями x(i). У узла есть два входа и один выход. 
- Для свёрточных как [NASNet](:/01eb668ead184c7aa10de9030bf8a800#nasnet).
- В случае рекуррентных один вход -- текущее состояние, второй состояние с предыдущего шага. Выход получается путём операции аггрегации выходов с узлов, например конкатенация.
![92ef9a0ef72940907be7b7b928258c17.png](../../_resources/d5bdb949c5e947d68087fd27223e4cc1.png)

# Continuous Relaxation and Optimization
Вводится некоторое множество возможных операторов O, например, (конволюция, пулинг, zero), каждый из которых можнно применить к состоянию x(i). Для того, чтобы сделать пространство поиска **непрерывным** ослабляется категориальность путём ввода softmax от всех возможных операций.
![75d78c3afc855afa0c74375587189536.png](../../_resources/c94da4406dbd46b29e0983522cba3579.png)
Итоговая операция для пары узлов (i,j) получается **смешиванием весов | взвешенной суммой(?!)   | mixing weights for a pair of nodes из формулы кажется взвешенным** и она параметризуется вектором a(i,j) длинной равной |O| количеству операций. Задача поиска архитектуры сводится к подбору множества непрерывных переменных *a* = {a(i,j)}, как на Fig.1 В конце поиска архитектура получается путём замены смешанной операции ~o(i,j), операцией с наибольшим значением соответствующего элемента из a. 
![a540609b6914c4ad16a7ee54482526e0.png](../../_resources/23ce2534873d48aab5d9ccf00b5e86db.png)
Также есть zero operation, означающая отсутсвие соединения между узлами. Поиск архитекутры сводится к поиску операций.
Архитектуры сравниваются на основе ошибки на валидационном наборе, на которое влияет не только *a*, но и веса *w* лучшие веса выбираются на основании тренировочной выборки 
![cdc7d1e5eb7b5e9468216ec81ce0b0a4.png](../../_resources/97412288d8554dcba987a3c6b9dc3835.png)
![d17df024916e1f25032fdcb1d1ab49cd.png](../../_resources/371c6b8906174908ba23a0284599970b.png)
# Approximate Architecture Gradient
Точная оценка -- дорого
![076f020dcecf12e32b62510964e107de.png](../../_resources/f36e50e7c79142aebd28fd5713f48c57.png)
*w* -- веса, *E(epsilon)* -- learning rate, идея в том, чтобы получить веса за один шаг тренировки не решая полностью вложенную задачу оптимизации весов (формула 4) тренеровкой до сходимости. Вдохновлялись они: [meta learning for model transfer](#meta-learning-for-model-transfer), [gradient-based hyperparameter tuning](#gradient-based-hyperparameter-tuning), [unrolled generative adversarial networks](unrolled-generative-adversarial-networks)
Часть, которую я не до конца понял
![3e43b76c375239a9470e21c84dc90999.png](../../_resources/96ba0113f7fb48c9a2b393f327fe6e5f.png)
*E* можно сделать 0, пишут, что это работает быстрее, однако, эмпирически, даёт худший результат. *E* = 0 => first order *E* != 0 => second order approximation.

# Deriving Discrete Architectures
Для формирования каждого узла в архитектуре сохраняют топ k сильнейшьих операций, из разных узлов среди всех ненулевых операций кандидатов собранных со всех предыдущих узлов. Сила операции по формуле ![109e28789c4ac5e1cbc606a29fa0d0af.png](../../_resources/b1df780c6ed3411d9836859ed8ddffbc.png)
для сохранения совместимости с текущими работами используют k=2 для ячеек вида [NASNet](:/01eb668ead184c7aa10de9030bf8a800#nasnet) и k=1 для рекурсивных [ENAS](#enas)
![3f83b2ab0d03511c9d7f08ab32f6b4db.png](../../_resources/52395ddf2cb84012a981db6b1d568ad0.png)

The zero operations are excluded in the above for two reasons. First, we need exactly k non-zero
incoming edges per node for fair comparison with the existing models. Second, the strength of the
zero operations is underdetermined, as increasing the logits of zero operations only affects the scale
of the resulting node representations, and does not affect the final classification outcome due to the
presence of batch normalization (Ioffe & Szegedy, 2015).

# Experiments and results
# links
## meta learning for model transfer
Chelsea Finn, Pieter Abbeel, and Sergey Levine. Model-agnostic meta-learning for fast adaptation of deep networks. In ICML, pp. 1126–1135, 2017
## gradient-based hyperparameter tuning
Jelena Luketina, Mathias Berglund, Klaus Greff, and Tapani Raiko. Scalable gradient-based tuning
of continuous regularization hyperparameters. In ICML, pp. 2952–2960, 2016.
## unrolled generative adversarial networks
Luke Metz, Ben Poole, David Pfau, and Jascha Sohl-Dickstein. Unrolled generative adversarial
networks. ICLR, 2017.
## ENAS
Hieu Pham, Melody Y Guan, Barret Zoph, Quoc V Le, and Jeff Dean. Efficient neural architecture
search via parameter sharing. ICML, 2018b.
[Оглавление](../../Papers/automl/Оглавление.md)