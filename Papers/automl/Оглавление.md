Оглавление

# Table of content
[toc]
# Arhicetcure Search
## NAS
- [arxiv](https://arxiv.org/abs/2005.11074)
- [my_notes](../../Papers/automl/NAS.md)
## NASNet
- [arxiv](https://arxiv.org/abs/1707.07012)
- [medium](https://sh-tsang.medium.com/review-nasnet-neural-architecture-search-network-image-classification-23139ea0425d)
- [my_notes](../../Papers/automl/NASNet.md)
## AmoebaNet
- [paperwithcode](https://paperswithcode.com/method/amoebanet)
- [my_notes](../../Papers/automl/AmoebaNet.md)
- [github pytorch](https://github.com/whiteio/AmoebaNet-Evolver)
## DARTS
- [arxiv](https://arxiv.org/abs/1806.09055)
- [gihub](https://github.com/quark0/darts)
- [my_notes](../../Papers/automl/DARTS%20%28Differentiable%20ARchiTecture%20Search%29.md)
- [math_begind](https://mythrex.github.io/math_behind_darts/)
## PC-DARTS
- [arxiv](https://arxiv.org/abs/1907.05737)
- [github](https://github.com/yuhuixu1993/PC-DARTS)
- [my_notes](../../Papers/automl/PC-DARTS_%20PARTIAL%20CHANNEL%20CONNECTIONS%20FOR%20MEMORY-E.md)
## Progressive-DARTS
- [arxiv](https://arxiv.org/abs/1912.10952)
- [github](https://github.com/chenxin061/pdarts)
- [paperwithcode](https://paperswithcode.com/paper/progressive-darts-bridging-the-optimization)
- [my_notes](../../Papers/automl/Progressive%20DARTS.md)
## ProxylessNAS
- [arxiv](https://arxiv.org/abs/1812.00332)
- [github](https://github.com/MIT-HAN-LAB/ProxylessNAS)
- [my_notes](../../Papers/automl/ProxylessNAS.md)
## OFA
- [arxiv](https://arxiv.org/abs/1908.09791)
- [github](https://github.com/mit-han-lab/once-for-all)
- [colab tutorial](https://colab.research.google.com/github/mit-han-lab/once-for-all/blob/master/tutorial/ofa.ipynb)
- [youtube](https://youtu.be/a_OeT8MXzWI "[ICLR 2020] Once for All: Train One Network and Specialize it for Efficient Deployment")
- есть модуль для питона `pip install ofa`
- [my_notes](../../Papers/automl/OFA%20ONCE-FOR-ALL_%20TRAIN%20ONE%20NETWORK%20AND%20SPE-%20CIALI.md)
## DetNAS
- [arxiv](https://arxiv.org/abs/1903.10979)
- [github](https://github.com/megvii-model/DetNAS)
- [my_notes](../../Papers/automl/DetNAS_%20Backbone%20Search%20for%20Object%20Detection.md)
## NAS-FPN
- [arxiv](https://arxiv.org/abs/1904.07392)
- [github заявлена поддержка этого метода](https://github.com/open-mmlab/mmdetection)
## MnasNet
- [arxiv](https://arxiv.org/abs/1807.11626)
- [github](https://github.com/tensorflow/tpu/tree/master/models/official/mnasnet)
## FBNet
- [arxiv](https://arxiv.org/abs/1812.03443)
- [github](https://github.com/facebookresearch/mobile-vision)
- [my_notes](../../Papers/automl/FBNet.md)
## BigNas
- [arxiv](https://arxiv.org/abs/2003.11142)
- [my_notes](../../Papers/automl/BigNAS.md)
## GAIA
- [arxiv](https://arxiv.org/abs/2106.11346)
- [my_notes](../../Papers/automl/GAIA.md)
## NSGANetV2
- [arxiv](https://arxiv.org/abs/2007.10396)
- [github](https://github.com/mikelzc1990/nsganetv2)
- [my_notes](../../Papers/automl/NSGANetV2%20%20%28MSuNAS%29.md)
## AttentiveNAS
- [arxiv](https://arxiv.org/abs/2011.09011)
- [github](https://github.com/facebookresearch/AttentiveNAS)
- [my_notes](../../Papers/automl/AttentiveNAS_%20Improving%20Neural%20Architecture%20Search.md)
## HardCoRe-NAS
- [arxiv](https://arxiv.org/abs/2102.11646)
## AlphaNet
- [arxiv](https://arxiv.org/abs/2102.07954)
## BossNas
- [arxiv](https://arxiv.org/abs/2103.12424)
# HPO
## Hyperband (Hyperband: A Novel Bandit-Based Approach to Hyperparameter Optimization)
[arxiv](https://arxiv.org/abs/1603.06560)
https://homes.cs.washington.edu/~jamieson/hyperband.html

## FABOLAS (Fast Bayesian Optimization of Machine Learning Hyperparameters on Large Datasets)
[arxiv](https://arxiv.org/abs/1605.07079)

## PBT (Population Based Training of Neural Networks)
[arxiv](https://arxiv.org/abs/1711.09846)
https://deepmind.com/blog/article/population-based-training-neural-networks

## BOHB (BOHB: Robust and Efficient Hyperparameter Optimization at Scale)
[arxiv](https://arxiv.org/abs/1807.01774)

## PAC-Bayes (Efficient Hyperparameter Optimization By Way Of PAC-Bayes Bound Minimization)
[arxiv](https://arxiv.org/abs/2008.06431)
[Оглавление](#table-of-content)
# outro
![4c1eca3b551e2660c53f8d3f93463014.png](../../_resources/07db4233d10e4c769523866c4a466d8e.png)
[OFA](../../Papers/automl/Оглавление.md#OFA)
Сравнили архитектуры добавив затраты на поиск архитектуры, последующее обучение и стоимость в CO2 и в деньгах (AWS) 
![3120a74f5c51694f948019729448519b.png](../../_resources/f9d481c0697345a88eb13c46c0bc5bb7.png)
# Возможные добавления
- ENAS
- One shot
- PNAS
- Сокращение Search Space. 
Ilija Radosavovic, Raj Prateek Kosaraju, Ross Girshick,
Kaiming He, and Piotr Doll ́ar. Designing network design
spaces. In CVPR, 2020Ilija Radosavovic, Raj Prateek Kosaraju, Ross Girshick,
Kaiming He, and Piotr Doll ́ar. Designing network design
spaces. In CVPR, 2020
