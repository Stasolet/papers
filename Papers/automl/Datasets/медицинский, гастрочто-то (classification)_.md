медицинский, гастрочто-то (classification)?

- медицинский, гастрочто-то (classification)?
	- [dataset](https://www.kaggle.com/plhalvorsen/kvasir-v2-a-gastrointestinal-tract-dataset)
- Классификация самолётов: (classification)
	- [paper](https://doi.org/10.1016/j.asoc.2020.106132)
	- [dataset](https://zenodo.org/record/3464319)
	- https://paperswithcode.com/dataset/fgvc-aircraft-1
- Тачки (key point)
	- [github](https://github.com/Pirazh/Vehicle_Key_Point_Orientation_Estimation)
- Тачки 2 key point, pose estimation
	- [dataset](http://apolloscape.auto/car_instance.html)
	- [paper with code](https://paperswithcode.com/paper/keypoint-communities)
- Тачки 3, pose estimation (MIT лицензия)
	- [paper with code](https://paperswithcode.com/dataset/vehicle-pose-estimation)
- Pose Estimation COCO dev
	- [dataset](https://cocodataset.org/#download)
- Pose Estimation MPII (MPII Human Pose)
	- [dataset](olab.research.google.com/)
- Hand Pose Estimation
	- [dataset](https://paperswithcode.com/dataset/nyu-hand)
- Мосты (точки Кластер)
- Самолёты (точки Кластер)

- Текстуры: https://paperswithcode.com/dataset/dtd