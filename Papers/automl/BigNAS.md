BigNAS

[Оглавление](../../Papers/automl/Оглавление.md)
Вдохновленно OFA, подход с самодистиляцией
![a9af9bc2c153c491c779af2dce14d6e2.png](../../_resources/4ad34c8b4187497586002eb63cb91481.png)
Средняя картинка эта OFA
Говорят, что после обучения OFA всё-равно на fine-tune делать для лучшей точности
*single stage model*
Есть сложности при её использовании, например при неправильной инициализации loss explode, большие дочерние сети переучиваются, в то время, как мелкие на плато.
Использовали inverted bottleneck residual и squeeze-and-excitation module (EfficienNet, MobileNetV3)
# Принципы.
**Sandwich Rule** на каждом шаге обучения случайно генерируется одна большая (вся сеть), одна наименьшая маленькая и N -- случайных, у авторов N=2. Их градиенты аггрегируются для итоговой коррекции весов всей модели
**Inplace Distilation** sotf labels от большой ко всем маленьким. Причём после аугментации (случайного кропа) вход для большой модели для генерации soft label бикубической интерполяцией переводится в разрешение дочерней модели. + 0.3% top1 acc/ 
**Initialization** выход каждого residual block (до skip connection) инициализирован нулевым тензором установкой learnable scaling coefficient 0, в последнем Batch Normalization. До этого они могли запускать обучение срезая LR на 30%, но теряя в итоге ~1% точности. Используют He Initialization.
![659c382989c850d1f0d4b087f40edd78.png](../../_resources/c6023d02731e47eb99dd9f38a8adbb5a.png)
## Convergence Behavior:
Мелкие модели сходятся медленнее больших на ImageNet, и если продолжать обучение для маленьких, то большие начинают переобучаться. Пробовали линейный decay и cosine decay, оказался хуже exponentially.
![945862af61081a6b02ce5dfca093a383.png](../../_resources/44bf387bfcf44020b8c1db680e073aa8.png)
Решают модификацией Exponentially decaying добавляя окончание обучения с константным LR, фиксируя его, когда он становится 5% от начального.
## Regularization
 Также для решения этой проблемы вводят регуляризацию, один из подходов это dropout для больших сетей, но в данной ситуации это было сложно. Ещё один подход -- weight decay. Они используют простое правило, показавшее эффективность на опыте: регуляризация применяется только к наибольшей (полной) модели. Применяют dropout и weight decay. Это работает потому что истинные метки видит только полная модель, остальные учатся дистиляцией
 ## Batch Norm Calibration.
 После обучения для каждой итоговой модели пересчитывают BN статистики
 # Coarse-to-fine Architecture Selection (от толст к тонк)
 Пространство поиска
 ![e6fa2f07c42705db8c190a1ece3ef35a.png](../../_resources/f266a1165b374a559f0e09667ea5a49e.png)
 Сначала выбирают некоторый скелет, а потом в окрестности случайными мутациями ищут архитектуры. 
 Есть некоторый изнальный конечный ограниченный набор возможжных конфигураций. Specifically, in the coarse selection phase, we
pre-define five input resolutions (network-wise, {192, 224, 256, 288, 320}), four depth
configurations (stage-wise via global depth multipliers ), two channel con-
figurations (stage-wise via global width multipliers) and four kernel size
configurations (stage-wise). Потом с учётом бюджета по вычислениям ищут лучшую сеть.
 Иллюстрация поиска
 ![29476e9312f57bbce8a6fc1c9c395b98.png](../../_resources/8d194d1b706241d49fa9a70d0371a947.png)
.
В плане глубины у сети есть семь уровней не включая первые и последний свёрточные слои. На каждом подбирается своя конфигурация в плане количества слоёв
Конфигурацию *search space* соствавляет
- Входное разрешение
- Количество слоёв
При разделении параметров 3х3 можно получить как центр 5х5
Они потом потестили обучение архитектур с нуля и fine tune, но оно не дало профита.
Реализация на TF, учили на TPU
# ImageNet results
![471fb8776a824ded163322490bafdf41.png](../../_resources/2dc3662e9ec44f8dac3faaf20f084be8.png)
[Оглавление](../../Papers/automl/Оглавление.md)

- Inplace distilation 
Yu, J., Huang, T.: Universally slimmable networks and improved training tech-
niques. In: 2019 IEEE/CVF International Conference on Computer Vision (ICCV).
pp. 1803–1811. IEEE (2019)
- Init model with zeros:
Goyal, P., Doll ́ar, P., Girshick, R., Noordhuis, P., Wesolowski, L., Kyrola, A., Tul-
loch, A., Jia, Y., He, K.: Accurate, large minibatch sgd: Training imagenet in 1
hour. arXiv preprint arXiv:1706.02677 (2017)
- AutoAugment
Cubuk, E.D., Zoph, B., Mane, D., Vasudevan, V., Le, Q.V.: Autoaugment: Learning
augmentation strategies from data. In: Proceedings of the IEEE conference on
computer vision and pattern recognition. pp. 113–123 (2019)