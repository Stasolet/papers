Pose estimation


- PoseNFS (Neural Fabrics Search)
	- https://arxiv.org/abs/1909.07068
- AutoSNAP (сомнительно, кода нет, для общего развития)
	- https://arxiv.org/abs/2006.14858
- AutoPose
	- https://arxiv.org/abs/2008.07018
- EfficientPose
	- https://arxiv.org/abs/2012.07086
- VipNas (На основе BIGNAS)
	- https://arxiv.org/abs/2105.10154
