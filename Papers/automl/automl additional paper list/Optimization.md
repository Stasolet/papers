Optimization

Another
- Inplace distilation 
Yu, J., Huang, T.: Universally slimmable networks and improved training tech-
niques. In: 2019 IEEE/CVF International Conference on Computer Vision (ICCV).
pp. 1803–1811. IEEE (2019)
- Init model with zeros:
Goyal, P., Doll ́ar, P., Girshick, R., Noordhuis, P., Wesolowski, L., Kyrola, A., Tul-
loch, A., Jia, Y., He, K.: Accurate, large minibatch sgd: Training imagenet in 1
hour. arXiv preprint arXiv:1706.02677 (2017)
- AutoAugment
Cubuk, E.D., Zoph, B., Mane, D., Vasudevan, V., Le, Q.V.: Autoaugment: Learning
augmentation strategies from data. In: Proceedings of the IEEE conference on
computer vision and pattern recognition. pp. 113–123 (2019)
- RegNet. оптимизация search space
lija Radosavovic, Raj Prateek Kosaraju, Ross Girshick,
Kaiming He, and Piotr Doll ́ar. Designing network design
spaces. In CVPR, 2020. 
