Detection

- Bfbox: Yang Liu and Xu Tang. Bfbox: Searching face-appropriate
backbone and feature pyramid network for face detector. In
CVPR, 2020.
- ASFD: Bin Zhang, Jian Li, Yabiao Wang, Ying Tai, Chengjie Wang,
Jilin Li, Feiyue Huang, Yili Xia, Wenjiang Pei, and Ron-
grong Ji. Asfd: Automatic and scalable face detector.
arXiv:2003.11228, 2020.
- CR-NAS Feng Liang, Chen Lin, Ronghao Guo, Ming Sun, Wei Wu,
Junjie Yan, and Wanli Ouyang. Computation reallocation
for object detection. In ICLR, 2020. 1, 2
Перераспределение вычислений между head, neck, backbone
- RegNet lija Radosavovic, Raj Prateek Kosaraju, Ross Girshick,
Kaiming He, and Piotr Doll ́ar. Designing network design
spaces. In CVPR, 2020. 
оптимизация search space