Evolution

- 1999 X. Yao. Evolving artificial neural networks. *Куча цитирования, выглядит как чисто теоретическая про эволюционные*
	- https://www.cs.bham.ac.uk/~xin/papers/published_iproc_sep99.pdf
- NEAT 2002
	- pdf http://citeseerx.ist.psu.edu/viewdoc/download doi=10.1.1.28.5457&rep=rep1&type=pdf
	- abs https://direct.mit.edu/evco/article-abstract/10/2/99/1123/Evolving-Neural-Networks-through-Augmenting?redirectedFrom=fulltext
	- https://paperswithcode.com/paper/evolving-neural-networks-through-augmenting
- D. Floreano, P. D  ̈urr, and C. Mattiussi. Neuroevolu-
tion: from architectures to learning. Evolutionary In-
telligence, 2008. *ещё одна цитируемая тоже более теоретическая про эволюцию для поиска архитектур, в целом*
	- https://link.springer.com/content/pdf/10.1007/s12065-007-0002-4.pdf
- LEMONADE
	- https://arxiv.org/abs/1804.09081
- The Evolved Transformer
	- https://arxiv.org/abs/1901.11117
- CARS: Continuous Evolution for Efficient Neural Architecture Search
	- https://arxiv.org/abs/1909.04977
- AutoML-Zero
	- https://arxiv.org/abs/2003.03384
- NPENAS  # быстро сходится ! 2080ti есть репа!!
	- https://arxiv.org/abs/2003.12857
- Evolving Reinforcement Learning Algorithms (как делать RL контроллеры эволюцией)
	- https://arxiv.org/abs/2101.03958
- ES-ENAS
	- https://arxiv.org/abs/2101.07415


- LargeEvoNet
- AgingEvoNet
- HierEvoNet
- MorphEvoNet





C. Liu, B. Zoph, M. Neumann, J. Shlens, W. Hua, L.-J. Li, L. Fei-Fei,
A. Yuille, J. Huang, and K. Murphy, “Progressive neural architecture
search,” in Computer Vision – ECCV 2018, V. Ferrari, M. Hebert,
C. Sminchisescu, and Y. Weiss, Eds. Springer International Publishing,
2018, pp. 19–35.