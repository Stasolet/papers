ProxylessNAS

[Оглавление](../../Papers/automl/Оглавление.md)
# intro
![e5b89330df045ddb72e8e13236ab951e.png](../../_resources/325d2f13cb7444f6af013801616ec700.png)
Тестировались, в том числе, в задачах под железо.
200 GPU hours, это типо меньше чем [MnasNet](../../Papers/automl/Оглавление.md#MnasNet), при сравнении на ImageNet.
Архитектурные параметры бинаризуются до [0,1], и только один путь активен, предлагается подход для тренировки бинарных параметров. Представление задержки сети как непрерывной функции и оптимизируется потери с регуляризацией.
* Первый подход с прямым обучением на датасете, без прокси, нет ограничений на повторение блоков.
* PathLevel pruning выкидывать незначимые пути, как в P-DARTS
* latency regularization loss для обучение под железо

# Learning Binarized Path

![be1207955d7f75827939f633f1e76a58.png](../../_resources/39ce34059c324f878d2f8cf0e5ce4c21.png)
![46fc97b8fa98d4b8e10b2ac3e4014232.png](../../_resources/5df74c62a23b4d398b765f291cfdc198.png)
По сути написано, что берётся и используется один путь, и его выход берётся как результат.
Обновляются веса слоя
# HANDLING NON-DIFFERENTIABLE HARDWARE METRICS
## MAKING LATENCY DIFFERENTIABLE
![a3ccfe0df53ceda5bfcc8c555ec98d5e.png](../../_resources/53774dc331f44b5dbd9348857b7fb92f.png)![8f7f7bb9469c26511244ef641ebcc21a.png](../../_resources/3170963be8f64852933f5ec5d8d16235.png)
Latency полагается как взвешенное произведение задержки на операторе и его вероятности.
![529bba7962e6c2c11e72f194c5045cef.png](../../_resources/89c09ce0619546dc958ed28325308041.png)
![4f4f50d7cf07162c5f8b5986986255c2.png](../../_resources/ad4f38952ba84ab0897e279cdb6cb25f.png)
![ee97e54c8e3060d6fa468b12390122dc.png](../../_resources/0831d40e10f047f6b7f2a671f2100813.png)
Также они предложили юзать Rl, версия с градиентом оказалась лучше RL, на CIFAR10. Однако версия RL оказалась лучше на ImageNet для мобильныхх, с топ 5 92.2 и задержкой 78 ms за 200 GPU hours

Они заменили 3х3 свёртки в residual block из PyramidNet ячейками с древовидной структурой, где каждая имеет по две ветки, кроме листовых, на основе [Path-Level Network Transformation for NAS](https://arxiv.org/pdf/1806.02639.pdf)
[Оглавление](../../Papers/automl/Оглавление.md)

Для оценки производительности модели на конечном железе использовалась модель, выбрали 25% на тест модели оценки производительнсоти сети (1 тысячу из 5)
![447d5b4fc983afab5a610ed7861473d3.png](../../_resources/9d797cdf967c420ead1b6e87c12a8051.png)
![14fb1cee7bf75f170046d613649bedcb.png](../../_resources/3ae955626d7a41c6a0b7ab5b1aa07655.png)